<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Create Event</title>
	</head>
	<c:set value="Create Event" var="headerText" scope="request" />
	<jsp:include page="pageHeader.jsp" />
		
		<div id="main">
			<c:url value="/event/create" var="formTarget" />
			<form action="${formTarget}" method="post">
				<p><label>Title</label>
				<input type="text" name="title" /></p>
				<p><label>City</label>
				<input type="text" name="city" /></p>
				<p><label>Content</label>
				<input type="text" name="content" /></p>
				<p><label>Start time</label>
				<input type="text" name="start_time">\"yy-mm-dd hh:mm\"</p>
				<p><label>Stop time</label>
				<input type="text" name="stop_time">\"yy-mm-dd hh:mm\"</p>
				<p><label>Organizer</label>
				<select id="dropbox" name="dropbox">
					<c:forEach items="${users}" var="user">
						<option value="${user.id}"><c:out value="${user.lastName}" /> <c:out value="${user.firstName}" /></option>
					</c:forEach>
				</select>
				<p>
					<input type="submit" value="Create Event" />
				</p>
			</form>
		</div>
				<div id="footer">
			<p>Page counter: <c:out value="${hits}" /></p>
		</div>	
		
	</body>
</html>