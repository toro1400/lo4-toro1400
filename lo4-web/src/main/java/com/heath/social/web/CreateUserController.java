package com.heath.social.web;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.heath.social.ejb.ISocialDAO;
import com.heath.social.model.User;

/**
 * Servlet implementation class CreateUserController
 */
@WebServlet("/user/create")
public class CreateUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AtomicInteger hits;
	
	@EJB
	private ISocialDAO dao;
	
	@Override
	public void init() {
		hits = new AtomicInteger(0);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/createUser.jsp");
		request.setAttribute("hits", hits.incrementAndGet());
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost 1");
		System.out.println(request.getParameter("email"));
		User user = new User(request.getParameter("email"), request.getParameter("first_name"),request.getParameter("last_name"));
		dao.addUser(user);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/a.jsp");
		rd.forward(request, response);
	}

}
