package com.heath.social.web;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.heath.social.ejb.ISocialDAO;
import com.heath.social.parser.Parser;

/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {

	@EJB
	private ISocialDAO dao;
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		InputStream is = null;
		try {
			URL inputFileURL = new URL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt");
			is = inputFileURL.openStream();
			Parser esp = new Parser(is);
			esp.parse();
			dao.persistAll(esp.getUsers(), esp.getEvents(), esp.getComments());
		} catch (Exception e) {
			System.err.println("Couldn't populate database: " + e.getMessage());
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (IOException e) {}
		}
	}
}
