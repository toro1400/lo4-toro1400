package com.heath.social.ejb;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.heath.social.model.Comment;
import com.heath.social.model.Event;
import com.heath.social.model.User;

@Stateless
public class SocialDAO {
	
	@PersistenceContext(unitName = "social")
	EntityManager em;
		
	@SuppressWarnings("unchecked")
	public List<Event> findAllEvents() {
		return (List<Event>) em.createQuery("SELECT e FROM Event e ORDER BY e.startTime").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		return (List<User>) em.createQuery("SELECT u FROM User u").getResultList();
	}
	
	public void addEvent(Event e) {
		em.persist(e);
	}
	
	public void addUser(User u) {
		em.persist(u);
	}
	
	public void persistAll(Set<User> users, Set<Event> events, Set<Comment> comments) {
		for (User u : users)
			em.persist(u);
		for (Event e : events)
			em.persist(e);
		for (Comment c : comments)
			em.persist(c);
	}
}
