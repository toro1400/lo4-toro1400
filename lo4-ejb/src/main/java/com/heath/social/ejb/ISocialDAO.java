package com.heath.social.ejb;

import java.util.List;
import java.util.Set;

import com.heath.social.model.Comment;
import com.heath.social.model.Event;
import com.heath.social.model.User;

public interface ISocialDAO {
	
	public List<Event> findAllEvents();
	public List<User> findAllUsers();
	public void addEvent(Event e);
	public void addUser(User u);
	public void persistAll(Set<User> users, Set<Event> events, Set<Comment> comments);
}
