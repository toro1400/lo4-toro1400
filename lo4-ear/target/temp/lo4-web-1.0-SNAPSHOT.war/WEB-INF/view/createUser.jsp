<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create User</title>
</head>
<body>
<c:url value="/user/create" var="formTarget" />
<form method="post" action="${formTarget}" id="create_user_form">
	<p>User data:</p>
	<p>
		<label>First name</label>
		<input type="text" name="first_name" />
	</p>
	<p>
		<label>Last name</label>
		<input type="text" name="last_name" />
	</p>
	<p>
		<label>Email</label>
		<input type="text" name="email" />
	</p>
	<input type="submit" name="add_user" />
</form>

</body>
</html>